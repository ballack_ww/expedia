# Expedia App

This application is used to display hotel offers through expedia webservice API , which can easily be deployed to Heroku.

This application supports the [Getting Started with Java on Heroku](https://ballack_ww@bitbucket.org/ballack_ww/expedia.git) - check it out.

## Running Locally

Make sure you have Java and Maven installed.  Also, install the [Heroku Toolbelt](https://toolbelt.heroku.com/).

```sh
$ git clone https://ballack_ww@bitbucket.org/ballack_ww/expedia.git
$ cd expedia
$ mvn install
$ heroku local:start
```

Your app should now be running on [localhost:5000](http://localhost:5000/).


## Deploying to Heroku

```sh
$ heroku create
$ git push heroku master
$ heroku open
```

## Documentation

For more information about using Java on Heroku, see these Dev Center articles:

- [Java on Heroku](https://devcenter.heroku.com/categories/java)
