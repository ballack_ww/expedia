package com.expedia.common.util;

import java.util.Collection;
import java.util.List;

/**
 * This Class used to handle Collection operation 
 */
public class CollectionUtil {

	private CollectionUtil() {
	}

	/**
	 * This method to check if Collection is Empty
	 * @param Collection list
	 * @return boolean
	 */
	public static boolean isEmptyList(Collection list) {
		if (list == null || list.size() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * This method to check if Collection is Not Empty
	 * @param Collection list
	 * @return boolean
	 */
	public static boolean isNotEmptyList(List list) {
		return !isEmptyList(list);
	}

}
