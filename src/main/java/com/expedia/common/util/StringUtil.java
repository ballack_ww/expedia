package com.expedia.common.util;

/**
 * This Class used to handle String operation 
 */
public class StringUtil {

	private StringUtil() {
	}

	/**
	 * This method to check if String is Empty
	 * @param String data
	 * @return boolean
	 */
	public static boolean isEmpty(String data) {
		if (data == null || data.trim().length() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * This method to check if String is NOT Empty
	 * @param String data
	 * @return boolean
	 */
	public static boolean isNotEmpty(String data) {
		if (isEmpty(data)) {
			return false;
		}
		return true;
	}

}
