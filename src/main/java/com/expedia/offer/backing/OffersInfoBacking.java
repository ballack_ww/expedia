package com.expedia.offer.backing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.primefaces.event.SelectEvent;

import com.expedia.common.util.CollectionUtil;
import com.expedia.common.util.StringUtil;
import com.expedia.offer.bean.Hotel;
import com.expedia.offer.bean.HotelInfo;
import com.expedia.offer.bean.OffersSearchBean;
import com.expedia.offer.bean.Root;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

@ManagedBean(name = "offersInfoBacking")
@ViewScoped

public class OffersInfoBacking {

	/**
	 * List Represent Return Hotels Offers 
	 */
	private List<Hotel> hotelsOfferList = new ArrayList<Hotel>();
	
	/**
	 * This Object used for Hotels offer search
	 */
	private OffersSearchBean searchHotel = new OffersSearchBean();

	/**
	 * PostConstruct to initiate Objects 
	 */
	@PostConstruct
	private void initSearchBean() {
		hotelsOfferList = new ArrayList<Hotel>();
		searchHotel = new OffersSearchBean();
	}

	/**
	 * @throws IOException
	 * This method used to populate hotels offers from API Webservice 
	 */
	public void populateOffers() throws IOException {

		DefaultHttpClient httpClient = new DefaultHttpClient();
		String apiUrl = buildApiUrl();
		HttpGet getRequest = new HttpGet(apiUrl);

		HttpResponse response = httpClient.execute(getRequest);

		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

		StringBuilder output = new StringBuilder();
		String line;
		while ((line = br.readLine()) != null) {
			output.append(line);
		}
		httpClient.getConnectionManager().shutdown();

		if (StringUtil.isNotEmpty(output.toString())) {
			convertJason(output.toString());
		}
	}
	
	/**
	 * This method used to build search Hotel offers API URL
	 * @return Offers API URL 
	 */
	private String buildApiUrl(){
		StringBuffer apiUrl = new StringBuffer( "https://offersvc.expedia.com/offers/v2/getOffers?scenario=deal-finder&page=foo&uid=foo&productType=Hotel");
		
		if(StringUtil.isNotEmpty(getSearchHotel().getCity())){
			apiUrl.append("&destinationCity="+getSearchHotel().getCity());
		}
		
		if(getSearchHotel().getMaxTripStartDate() !=null){
			apiUrl.append("&maxTripStartDate="+new SimpleDateFormat("yyyy-MM-dd").format(getSearchHotel().getMaxTripStartDate()));
		}
		
		if(getSearchHotel().getMinTripStartDate() !=null){
			apiUrl.append("&minTripStartDate="+new SimpleDateFormat("yyyy-MM-dd").format(getSearchHotel().getMinTripStartDate()));
		}
		
		if(getSearchHotel().getLengthOfStay() != null){
			apiUrl.append("&lengthOfStay="+getSearchHotel().getLengthOfStay());
		}
		
		if(getSearchHotel().getMinStarRating() != null){
			apiUrl.append("&minStarRating="+getSearchHotel().getMinStarRating());
		}
		
		if(getSearchHotel().getMaxStarRating() != null){
			apiUrl.append("&maxStarRating="+getSearchHotel().getMaxStarRating());
		}
		
		if(getSearchHotel().getMinTotalRate() != null){
			apiUrl.append("&minTotalRate="+getSearchHotel().getMinTotalRate());
		}
		
		if(getSearchHotel().getMaxTotalRate() != null){
			apiUrl.append("&maxTotalRate="+getSearchHotel().getMaxTotalRate());
		}
		
		if(getSearchHotel().getMinGuestRating() != null){
			apiUrl.append("&minGuestRating="+getSearchHotel().getMinGuestRating());
		}
		
		if(getSearchHotel().getMaxGuestRating() != null){
			apiUrl.append("&maxGuestRating="+getSearchHotel().getMaxGuestRating());
		}
		
		return apiUrl.toString();
	}

	
	/**
	 * This method used to convert jsonobject to JAVA object
	 * @param Hotel Offers result as jsonobject
	 */
	private void convertJason(String jsonobject) {
		hotelsOfferList = new ArrayList<Hotel>();
		Root offerInfo = new Root();
		Gson gson = new Gson();
		offerInfo = gson.fromJson(jsonobject, Root.class);
		if(offerInfo.getOffers() != null){
			if(CollectionUtil.isNotEmptyList(offerInfo.getOffers().getHotel())){
				hotelsOfferList.addAll(offerInfo.getOffers().getHotel());
			}
		}		
	}
	
	public void reset(){
		hotelsOfferList = new ArrayList<Hotel>();
		searchHotel = new OffersSearchBean();
	}
	
	
	/**
	 * @return Offers List
	 */
	public List<Hotel> getHotelsOfferList() {
		return hotelsOfferList;
	}

	/**
	 * @param hotelsOfferList
	 */
	public void setHotelsOfferList(List<Hotel> hotelsOfferList) {
		this.hotelsOfferList = hotelsOfferList;
	}

	/**
	 * @return
	 */
	public OffersSearchBean getSearchHotel() {
		return searchHotel;
	}

	/**
	 * @param searchHotel
	 */
	public void setSearchHotel(OffersSearchBean searchHotel) {
		this.searchHotel = searchHotel;
	}

}
