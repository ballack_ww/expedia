package com.expedia.offer.bean;

public class Destination {

	  private String regionID;

	    private String longName;

	    private String country;

	    private String province;

	    private String city;

	    public void setRegionID(String regionID){
	        this.regionID = regionID;
	    }
	    public String getRegionID(){
	        return this.regionID;
	    }
	    public void setLongName(String longName){
	        this.longName = longName;
	    }
	    public String getLongName(){
	        return this.longName;
	    }
	    public void setCountry(String country){
	        this.country = country;
	    }
	    public String getCountry(){
	        return this.country;
	    }
	    public void setProvince(String province){
	        this.province = province;
	    }
	    public String getProvince(){
	        return this.province;
	    }
	    public void setCity(String city){
	        this.city = city;
	    }
	    public String getCity(){
	        return this.city;
	    }

}
