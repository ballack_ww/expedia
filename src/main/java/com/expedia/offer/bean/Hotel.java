package com.expedia.offer.bean;


public class Hotel {

	private OfferDateRange offerDateRange;

    private Destination destination;

    private HotelInfo hotelInfo;

    private HotelUrgencyInfo hotelUrgencyInfo;

    private HotelPricingInfo hotelPricingInfo;

    private HotelUrls hotelUrls;

    private HotelScores hotelScores;

    public void setOfferDateRange(OfferDateRange offerDateRange){
        this.offerDateRange = offerDateRange;
    }
    public OfferDateRange getOfferDateRange(){
        return this.offerDateRange;
    }
    public void setDestination(Destination destination){
        this.destination = destination;
    }
    public Destination getDestination(){
        return this.destination;
    }
    public void setHotelInfo(HotelInfo hotelInfo){
        this.hotelInfo = hotelInfo;
    }
    public HotelInfo getHotelInfo(){
        return this.hotelInfo;
    }
    public void setHotelUrgencyInfo(HotelUrgencyInfo hotelUrgencyInfo){
        this.hotelUrgencyInfo = hotelUrgencyInfo;
    }
    public HotelUrgencyInfo getHotelUrgencyInfo(){
        return this.hotelUrgencyInfo;
    }
    public void setHotelPricingInfo(HotelPricingInfo hotelPricingInfo){
        this.hotelPricingInfo = hotelPricingInfo;
    }
    public HotelPricingInfo getHotelPricingInfo(){
        return this.hotelPricingInfo;
    }
    public void setHotelUrls(HotelUrls hotelUrls){
        this.hotelUrls = hotelUrls;
    }
    public HotelUrls getHotelUrls(){
        return this.hotelUrls;
    }
    public void setHotelScores(HotelScores hotelScores){
        this.hotelScores = hotelScores;
    }
    public HotelScores getHotelScores(){
        return this.hotelScores;
    }

}
