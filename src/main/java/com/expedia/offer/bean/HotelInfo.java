package com.expedia.offer.bean;

public class HotelInfo {

	  private String hotelId;

	    private String hotelName;

	    private String hotelDestination;

	    private String hotelDestinationRegionID;

	    private String hotelLongDestination;

	    private String hotelStreetAddress;

	    private String hotelCity;

	    private String hotelProvince;

	    private String hotelCountryCode;

	    private String hotelLocation;

	    private double hotelLatitude;

	    private double hotelLongitude;

	    private String hotelStarRating;

	    private double hotelGuestReviewRating;

	    private String travelStartDate;

	    private String travelEndDate;

	    private String hotelImageUrl;

	    private double carPackageScore;

	    private String description;

	    private int distanceFromUser;

	    private String language;

	    private double movingAverageScore;

	    private double promotionAmount;

	    private String promotionDescription;

	    private String promotionTag;

	    private double rawAppealScore;

	    private int relevanceScore;

	    private String statusCode;

	    private String statusDescription;

	    private boolean carPackage;

	    private boolean allInclusive;

	    public void setHotelId(String hotelId){
	        this.hotelId = hotelId;
	    }
	    public String getHotelId(){
	        return this.hotelId;
	    }
	    public void setHotelName(String hotelName){
	        this.hotelName = hotelName;
	    }
	    public String getHotelName(){
	        return this.hotelName;
	    }
	    public void setHotelDestination(String hotelDestination){
	        this.hotelDestination = hotelDestination;
	    }
	    public String getHotelDestination(){
	        return this.hotelDestination;
	    }
	    public void setHotelDestinationRegionID(String hotelDestinationRegionID){
	        this.hotelDestinationRegionID = hotelDestinationRegionID;
	    }
	    public String getHotelDestinationRegionID(){
	        return this.hotelDestinationRegionID;
	    }
	    public void setHotelLongDestination(String hotelLongDestination){
	        this.hotelLongDestination = hotelLongDestination;
	    }
	    public String getHotelLongDestination(){
	        return this.hotelLongDestination;
	    }
	    public void setHotelStreetAddress(String hotelStreetAddress){
	        this.hotelStreetAddress = hotelStreetAddress;
	    }
	    public String getHotelStreetAddress(){
	        return this.hotelStreetAddress;
	    }
	    public void setHotelCity(String hotelCity){
	        this.hotelCity = hotelCity;
	    }
	    public String getHotelCity(){
	        return this.hotelCity;
	    }
	    public void setHotelProvince(String hotelProvince){
	        this.hotelProvince = hotelProvince;
	    }
	    public String getHotelProvince(){
	        return this.hotelProvince;
	    }
	    public void setHotelCountryCode(String hotelCountryCode){
	        this.hotelCountryCode = hotelCountryCode;
	    }
	    public String getHotelCountryCode(){
	        return this.hotelCountryCode;
	    }
	    public void setHotelLocation(String hotelLocation){
	        this.hotelLocation = hotelLocation;
	    }
	    public String getHotelLocation(){
	        return this.hotelLocation;
	    }
	    public void setHotelLatitude(double hotelLatitude){
	        this.hotelLatitude = hotelLatitude;
	    }
	    public double getHotelLatitude(){
	        return this.hotelLatitude;
	    }
	    public void setHotelLongitude(double hotelLongitude){
	        this.hotelLongitude = hotelLongitude;
	    }
	    public double getHotelLongitude(){
	        return this.hotelLongitude;
	    }
	    public void setHotelStarRating(String hotelStarRating){
	        this.hotelStarRating = hotelStarRating;
	    }
	    public String getHotelStarRating(){
	        return this.hotelStarRating;
	    }
	    public void setHotelGuestReviewRating(double hotelGuestReviewRating){
	        this.hotelGuestReviewRating = hotelGuestReviewRating;
	    }
	    public double getHotelGuestReviewRating(){
	        return this.hotelGuestReviewRating;
	    }
	    public void setTravelStartDate(String travelStartDate){
	        this.travelStartDate = travelStartDate;
	    }
	    public String getTravelStartDate(){
	        return this.travelStartDate;
	    }
	    public void setTravelEndDate(String travelEndDate){
	        this.travelEndDate = travelEndDate;
	    }
	    public String getTravelEndDate(){
	        return this.travelEndDate;
	    }
	    public void setHotelImageUrl(String hotelImageUrl){
	        this.hotelImageUrl = hotelImageUrl;
	    }
	    public String getHotelImageUrl(){
	        return this.hotelImageUrl;
	    }
	    public void setCarPackageScore(double carPackageScore){
	        this.carPackageScore = carPackageScore;
	    }
	    public double getCarPackageScore(){
	        return this.carPackageScore;
	    }
	    public void setDescription(String description){
	        this.description = description;
	    }
	    public String getDescription(){
	        return this.description;
	    }
	    public void setDistanceFromUser(int distanceFromUser){
	        this.distanceFromUser = distanceFromUser;
	    }
	    public int getDistanceFromUser(){
	        return this.distanceFromUser;
	    }
	    public void setLanguage(String language){
	        this.language = language;
	    }
	    public String getLanguage(){
	        return this.language;
	    }
	    public void setMovingAverageScore(double movingAverageScore){
	        this.movingAverageScore = movingAverageScore;
	    }
	    public double getMovingAverageScore(){
	        return this.movingAverageScore;
	    }
	    public void setPromotionAmount(double promotionAmount){
	        this.promotionAmount = promotionAmount;
	    }
	    public double getPromotionAmount(){
	        return this.promotionAmount;
	    }
	    public void setPromotionDescription(String promotionDescription){
	        this.promotionDescription = promotionDescription;
	    }
	    public String getPromotionDescription(){
	        return this.promotionDescription;
	    }
	    public void setPromotionTag(String promotionTag){
	        this.promotionTag = promotionTag;
	    }
	    public String getPromotionTag(){
	        return this.promotionTag;
	    }
	    public void setRawAppealScore(double rawAppealScore){
	        this.rawAppealScore = rawAppealScore;
	    }
	    public double getRawAppealScore(){
	        return this.rawAppealScore;
	    }
	    public void setRelevanceScore(int relevanceScore){
	        this.relevanceScore = relevanceScore;
	    }
	    public int getRelevanceScore(){
	        return this.relevanceScore;
	    }
	    public void setStatusCode(String statusCode){
	        this.statusCode = statusCode;
	    }
	    public String getStatusCode(){
	        return this.statusCode;
	    }
	    public void setStatusDescription(String statusDescription){
	        this.statusDescription = statusDescription;
	    }
	    public String getStatusDescription(){
	        return this.statusDescription;
	    }
	    public void setCarPackage(boolean carPackage){
	        this.carPackage = carPackage;
	    }
	    public boolean getCarPackage(){
	        return this.carPackage;
	    }
	    public void setAllInclusive(boolean allInclusive){
	        this.allInclusive = allInclusive;
	    }
	    public boolean getAllInclusive(){
	        return this.allInclusive;
	    }

}
