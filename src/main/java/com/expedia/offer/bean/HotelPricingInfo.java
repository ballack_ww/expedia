package com.expedia.offer.bean;

public class HotelPricingInfo {
	  private double averagePriceValue;

	    private double totalPriceValue;

	    private String originalPricePerNight;

	    private double hotelTotalBaseRate;

	    private double hotelTotalTaxesAndFees;

	    private String currency;

	    private double hotelTotalMandatoryTaxesAndFees;

	    private double percentSavings;
	    
	    private double hotelTotalRate;

	    private boolean drr;

	    public void setAveragePriceValue(double averagePriceValue){
	        this.averagePriceValue = averagePriceValue;
	    }
	    public double getAveragePriceValue(){
	        return this.averagePriceValue;
	    }
	    public void setTotalPriceValue(double totalPriceValue){
	        this.totalPriceValue = totalPriceValue;
	    }
	    public double getTotalPriceValue(){
	        return this.totalPriceValue;
	    }
	    public void setOriginalPricePerNight(String originalPricePerNight){
	        this.originalPricePerNight = originalPricePerNight;
	    }
	    public String getOriginalPricePerNight(){
	        return this.originalPricePerNight;
	    }
	    public void setHotelTotalBaseRate(double hotelTotalBaseRate){
	        this.hotelTotalBaseRate = hotelTotalBaseRate;
	    }
	    public double getHotelTotalBaseRate(){
	        return this.hotelTotalBaseRate;
	    }
	    public void setHotelTotalTaxesAndFees(double hotelTotalTaxesAndFees){
	        this.hotelTotalTaxesAndFees = hotelTotalTaxesAndFees;
	    }
	    public double getHotelTotalTaxesAndFees(){
	        return this.hotelTotalTaxesAndFees;
	    }
	    public void setCurrency(String currency){
	        this.currency = currency;
	    }
	    public String getCurrency(){
	        return this.currency;
	    }
	    public void setHotelTotalMandatoryTaxesAndFees(double hotelTotalMandatoryTaxesAndFees){
	        this.hotelTotalMandatoryTaxesAndFees = hotelTotalMandatoryTaxesAndFees;
	    }
	    public double getHotelTotalMandatoryTaxesAndFees(){
	        return this.hotelTotalMandatoryTaxesAndFees;
	    }
	    public void setPercentSavings(double percentSavings){
	        this.percentSavings = percentSavings;
	    }
	    public double getPercentSavings(){
	        return this.percentSavings;
	    }
	    public void setDrr(boolean drr){
	        this.drr = drr;
	    }
	    public boolean getDrr(){
	        return this.drr;
	    }
		public double getHotelTotalRate() {
			return hotelTotalRate;
		}
		public void setHotelTotalRate(double hotelTotalRate) {
			this.hotelTotalRate = hotelTotalRate;
		}
	    


}
