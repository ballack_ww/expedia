package com.expedia.offer.bean;

public class HotelScores {

	private double rawAppealScore;

    private double movingAverageScore;

    public void setRawAppealScore(double rawAppealScore){
        this.rawAppealScore = rawAppealScore;
    }
    public double getRawAppealScore(){
        return this.rawAppealScore;
    }
    public void setMovingAverageScore(double movingAverageScore){
        this.movingAverageScore = movingAverageScore;
    }
    public double getMovingAverageScore(){
        return this.movingAverageScore;
    }

}
