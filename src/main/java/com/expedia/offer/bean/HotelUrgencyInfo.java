package com.expedia.offer.bean;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class HotelUrgencyInfo {
	
	 private int airAttachRemainingTime;

	    private int numberOfPeopleViewing;

	    private int numberOfPeopleBooked;

	    private int numberOfRoomsLeft;

	    private BigInteger lastBookedTime;

	    private String almostSoldStatus;

	    private String link;

	    private List<String> almostSoldOutRoomTypeInfoCollection;

	    private boolean airAttachEnabled;

	    public void setAirAttachRemainingTime(int airAttachRemainingTime){
	        this.airAttachRemainingTime = airAttachRemainingTime;
	    }
	    public int getAirAttachRemainingTime(){
	        return this.airAttachRemainingTime;
	    }
	    public void setNumberOfPeopleViewing(int numberOfPeopleViewing){
	        this.numberOfPeopleViewing = numberOfPeopleViewing;
	    }
	    public int getNumberOfPeopleViewing(){
	        return this.numberOfPeopleViewing;
	    }
	    public void setNumberOfPeopleBooked(int numberOfPeopleBooked){
	        this.numberOfPeopleBooked = numberOfPeopleBooked;
	    }
	    public int getNumberOfPeopleBooked(){
	        return this.numberOfPeopleBooked;
	    }
	    public void setNumberOfRoomsLeft(int numberOfRoomsLeft){
	        this.numberOfRoomsLeft = numberOfRoomsLeft;
	    }
	    public int getNumberOfRoomsLeft(){
	        return this.numberOfRoomsLeft;
	    }
	    public void setLastBookedTime(BigInteger lastBookedTime){
	        this.lastBookedTime = lastBookedTime;
	    }
	    public BigInteger getLastBookedTime(){
	        return this.lastBookedTime;
	    }
	    public void setAlmostSoldStatus(String almostSoldStatus){
	        this.almostSoldStatus = almostSoldStatus;
	    }
	    public String getAlmostSoldStatus(){
	        return this.almostSoldStatus;
	    }
	    public void setLink(String link){
	        this.link = link;
	    }
	    public String getLink(){
	        return this.link;
	    }
	    public void setAlmostSoldOutRoomTypeInfoCollection(List<String> almostSoldOutRoomTypeInfoCollection){
	        this.almostSoldOutRoomTypeInfoCollection = almostSoldOutRoomTypeInfoCollection;
	    }
	    public List<String> getAlmostSoldOutRoomTypeInfoCollection(){
	        return this.almostSoldOutRoomTypeInfoCollection;
	    }
	    public void setAirAttachEnabled(boolean airAttachEnabled){
	        this.airAttachEnabled = airAttachEnabled;
	    }
	    public boolean getAirAttachEnabled(){
	        return this.airAttachEnabled;
	    }


}
