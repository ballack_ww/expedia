package com.expedia.offer.bean;

public class HotelUrls {
	
	private String hotelInfositeUrl;

    private String hotelSearchResultUrl;

    public void setHotelInfositeUrl(String hotelInfositeUrl){
        this.hotelInfositeUrl = hotelInfositeUrl;
    }
    public String getHotelInfositeUrl(){
        return this.hotelInfositeUrl;
    }
    public void setHotelSearchResultUrl(String hotelSearchResultUrl){
        this.hotelSearchResultUrl = hotelSearchResultUrl;
    }
    public String getHotelSearchResultUrl(){
        return this.hotelSearchResultUrl;
    }
}
