package com.expedia.offer.bean;

import java.util.List;

public class OfferDateRange {

	private List<Integer> travelStartDate;

    private List<Integer> travelEndDate;

    private int lengthOfStay;

    public void setTravelStartDate(List<Integer> travelStartDate){
        this.travelStartDate = travelStartDate;
    }
    public List<Integer> getTravelStartDate(){
        return this.travelStartDate;
    }
    public void setTravelEndDate(List<Integer> travelEndDate){
        this.travelEndDate = travelEndDate;
    }
    public List<Integer> getTravelEndDate(){
        return this.travelEndDate;
    }
    public void setLengthOfStay(int lengthOfStay){
        this.lengthOfStay = lengthOfStay;
    }
    public int getLengthOfStay(){
        return this.lengthOfStay;
    }

}
