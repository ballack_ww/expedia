package com.expedia.offer.bean;


public class OfferInfo {
	
	private String siteID;

	private String language;

    private String currency;

    public void setSiteID(String siteID){
        this.siteID = siteID;
    }
    public String getSiteID(){
        return this.siteID;
    }
    public void setLanguage(String language){
        this.language = language;
    }
    public String getLanguage(){
        return this.language;
    }
    public void setCurrency(String currency){
        this.currency = currency;
    }
    public String getCurrency(){
        return this.currency;
    }
}
