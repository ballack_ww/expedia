package com.expedia.offer.bean;

import java.util.Date;

public class OffersSearchBean {
	
	private String city;
	private Date minTripStartDate;
	private Date maxTripStartDate;
	private Integer lengthOfStay;
	private Double minStarRating;
	private Double maxStarRating;
	private Double minTotalRate;
	private Double maxTotalRate;
	private Double minGuestRating;
	private Double maxGuestRating;
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Date getMinTripStartDate() {
		return minTripStartDate;
	}
	public void setMinTripStartDate(Date minTripStartDate) {
		this.minTripStartDate = minTripStartDate;
	}
	public Date getMaxTripStartDate() {
		return maxTripStartDate;
	}
	public void setMaxTripStartDate(Date maxTripStartDate) {
		this.maxTripStartDate = maxTripStartDate;
	}
	public Integer getLengthOfStay() {
		return lengthOfStay;
	}
	public void setLengthOfStay(Integer lengthOfStay) {
		this.lengthOfStay = lengthOfStay;
	}
	public Double getMinStarRating() {
		return minStarRating;
	}
	public void setMinStarRating(Double minStarRating) {
		this.minStarRating = minStarRating;
	}
	public Double getMaxStarRating() {
		return maxStarRating;
	}
	public void setMaxStarRating(Double maxStarRating) {
		this.maxStarRating = maxStarRating;
	}
	public Double getMinTotalRate() {
		return minTotalRate;
	}
	public void setMinTotalRate(Double minTotalRate) {
		this.minTotalRate = minTotalRate;
	}
	public Double getMaxTotalRate() {
		return maxTotalRate;
	}
	public void setMaxTotalRate(Double maxTotalRate) {
		this.maxTotalRate = maxTotalRate;
	}
	public Double getMinGuestRating() {
		return minGuestRating;
	}
	public void setMinGuestRating(Double minGuestRating) {
		this.minGuestRating = minGuestRating;
	}
	public Double getMaxGuestRating() {
		return maxGuestRating;
	}
	public void setMaxGuestRating(Double maxGuestRating) {
		this.maxGuestRating = maxGuestRating;
	}
	
	
	

}
