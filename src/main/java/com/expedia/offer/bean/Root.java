package com.expedia.offer.bean;

public class Root
{
    private OfferInfo offerInfo;

    private UserInfo userInfo;

    private Offers offers;

    public void setOfferInfo(OfferInfo offerInfo){
        this.offerInfo = offerInfo;
    }
    public OfferInfo getOfferInfo(){
        return this.offerInfo;
    }
    public void setUserInfo(UserInfo userInfo){
        this.userInfo = userInfo;
    }
    public UserInfo getUserInfo(){
        return this.userInfo;
    }
    public void setOffers(Offers offers){
        this.offers = offers;
    }
    public Offers getOffers(){
        return this.offers;
    }
}
