package com.expedia.offer.bean;

public class UserInfo {

	 private Persona persona;

	    private String userId;

	    public void setPersona(Persona persona){
	        this.persona = persona;
	    }
	    public Persona getPersona(){
	        return this.persona;
	    }
	    public void setUserId(String userId){
	        this.userId = userId;
	    }
	    public String getUserId(){
	        return this.userId;
	    }
}
